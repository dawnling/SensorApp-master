package hades.org.sensorapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
    private Button compass_bt;
    private Button spirit_bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        compass_bt = (Button) findViewById(R.id.start_compass);
        spirit_bt = (Button) findViewById(R.id.start_spirit);

        int x = 100, y = 200, x1 = 100, y1 = 150;
        Log.e("角度", "角度1：" + Math.atan2((x - x1), (y - y1)) * (180 / Math.PI));//-125
        Log.e("角度", "角度2：" + Math.atan2((x - x1), (y1 - y)) * (180 / Math.PI));//-54
        Log.e("角度", "角度3：" + Math.atan2((x1 - x), (y - y1)) * (180 / Math.PI));//125
        Log.e("角度", "角度4：" + Math.atan2((x1 - x), (y1 - y)) * (180 / Math.PI));//54
        Log.e("角度", "角度5：" + Math.atan2((y - y1), (x - x1)) * (180 / Math.PI));//-144
        Log.e("角度", "角度6：" + Math.atan2((y - y1), (x1 - x)) * (180 / Math.PI));//-35
        Log.e("角度", "角度7：" + Math.atan2((y1 - y), (x - x1)) * (180 / Math.PI));//144
        Log.e("角度", "角度8：" + Math.atan2((y1 - y), (x1 - x)) * (180 / Math.PI));//35
    }

    public void starter(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.start_compass:
                Toast.makeText(this, "正在启动指南针程序", Toast.LENGTH_SHORT).show();
                intent.setClass(this, Compass.class);
                break;
            case R.id.start_spirit:
                Toast.makeText(this, "正在启动水平仪程序", Toast.LENGTH_SHORT).show();
                intent.setClass(this, Spirit.class);
                break;
            case R.id.start_gyroscopeSensor:
                Toast.makeText(this, "正在启动陀螺仪程序", Toast.LENGTH_SHORT).show();
                intent.setClass(this, GyroscopeSensorActivity.class);
                break;
            default:
                break;
        }
        startActivity(intent);
    }
}
