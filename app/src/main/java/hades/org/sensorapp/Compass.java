package hades.org.sensorapp;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Hades on 16/10/8.
 */
public class Compass extends Activity implements SensorEventListener{
    private EditText etStartX;
    private EditText etStartY;
    private EditText etEndX;
    private EditText etEndY;
    private TextView tvLines;
    private TextView tvLinesSo;
    private TextView tvCompass;
    //定义显示指南针的图片
    private ImageView znz_iv;
    //Sensor管理器
    private SensorManager sensorManager;
    //定义转过的角度
    private float currentDegree = 0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compass);
        znz_iv = (ImageView) findViewById(R.id.znz_iv);
        tvCompass = (TextView) findViewById(R.id.tvCompass);
        tvLines = (TextView) findViewById(R.id.tvLines);
        tvLinesSo = (TextView) findViewById(R.id.tvLinesSo);
        etStartX = (EditText) findViewById(R.id.etStartX);
        etStartY = (EditText) findViewById(R.id.etStartY);
        etEndX = (EditText) findViewById(R.id.etEndX);
        etEndY = (EditText) findViewById(R.id.etEndY);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        tvLines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String startX = etStartX.getText().toString().trim();
                String startY = etStartY.getText().toString().trim();
                String endX = etEndX.getText().toString().trim();
                String endY = etEndY.getText().toString().trim();
                soAngle = (float) (Math.atan2((Float.parseFloat(endX) - Float.parseFloat(startX)),
                        (Float.parseFloat(endY) - Float.parseFloat(startY)))*(180/Math.PI));
                tvLines.setText("起点终点线夹角：" + soAngle);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //为系统注册方向传感器监听器
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
    }

    private float soAngle = 0f;//so库算出 得到的角度
    private float oldDegree = 0f;
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        //获取触发event的传感器类型
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ORIENTATION:
                float newDegree = sensorEvent.values[0];
                if (Math.abs(newDegree - oldDegree) > 1){
                    oldDegree = newDegree;
                }
                //获取绕Z轴转过的角度
                float degree = oldDegree;
                tvCompass.setText("指南针夹角：" + degree);
                degree -= soAngle;
                //创建旋转动画 toDegrees:结束的角度度数，负数逆时针，正数顺时针
                RotateAnimation ra = new RotateAnimation(currentDegree, -degree
                        , Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                //设置动画持续时间
                ra.setDuration(200);
                //运行动画
                znz_iv.startAnimation(ra);
                //更新当前角度
                currentDegree = -degree;
                tvLinesSo.setText("最终夹角：" + degree + "  /最终旋转角：" + currentDegree);
                break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onPause() {
        //取消注册
        sensorManager.unregisterListener(this);
        super.onPause();
    }

    @Override
    protected void onStop() {
        //取消注册
        sensorManager.unregisterListener(this);
        super.onStop();
    }
}
